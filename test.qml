
import Qt 4.7
import "hildon"

HildonWindow {
    width: 800
    height: 480

    HildonButton {
        id: checkButton
        icon: "general_refresh"
        width: parent.width/2
        text: "Check for new episodes"
        onClicked: { thumb = !thumb }
        z: 2
        sensitive: false
    }

    HildonButton {
        id: addButton
        icon: "general_add"
        x: parent.width/2
        width: parent.width/2
        text: "Add new podcast"
        z: 2
        onClicked: { testDialog.appear() }
    }

    ListView {
        z: 1
        anchors {
            top: addButton.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        model: [
            "Even though it looks like Hildon",
            "It's using QML and Hildon theme graphics",
            "Even though it looks like Hildon",
            "It's using QML and Hildon theme graphics",
            "Even though it looks like Hildon",
            "It's using QML and Hildon theme graphics",
            "Even though it looks like Hildon",
            "It's using QML and Hildon theme graphics",
            "Even though it looks like Hildon",
            "It's using QML and Hildon theme graphics",
            "Even though it looks like Hildon",
            "It's using QML and Hildon theme graphics",
        ]

        delegate: HildonTouchListRow {
            HildonLabel {
                anchors.fill: parent
                text: modelData
            }
        }
    }

    HildonDialog {
        id: testDialog
        title: "Hey Ho Lets Go"
        actions: ["Delete", "Save"]
        dialogHeight: 300


        Flickable {
            anchors.fill: parent
            clip: true
            contentHeight: repeater.model * 100
            Repeater {
                id: repeater

                model: 12

                HildonButton {
                    text: modelData
                    y: index*100
                    width: 200
                    onClicked: { recursive.title = "Clicked on " + index; recursive.appear() }
                }
            }
            HildonLabel { text: "Burrup!"; x: 300; y: 400 }
        }
    }

    HildonDialog {
        id: recursive
        title: "HOHO"
        dialogHeight: 200
    }
}

